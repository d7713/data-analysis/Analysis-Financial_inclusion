# Financial service inclusion - Analysis

### By: Andrew Wairegi

## Description
To find out the most likely individuals to have a bank account. So that we can predict who
will have a bank account, based on features. This will allow us to find out the main predictors,
that indicate whether someone has a bacnk acount or not. This will allow the countries to determine which factors 
need to be considered, if they want to make it more financially inclusive.
<br><br>
I will do: preview, cleaning, data analysis, and model creation.
Then I will provide a conclusion, and evaluate the model.

[Open notebook]

## Setup/installation instructions
1. Find a folder
2. Setup the folder as a local repository
3. Clone the remote repository into the local repository (using the github link)
4. Open the collaboratory notebook
5. Upload the dataset file to the collaboratory file upload section
6. Run the notebook

## Known Bugs
There are no known bugs / issues

## Technologies Used
1. Python - language
2. Numpy - Arithmetic Package
3. Pandas - Data analysis Package
4. Seaborn - Visualization package
5. Matplotlib - Visualization package
6. FactorAnalyzer - Statistical Package
7. Scikit learn - Model package

<br>

### License
Copyright © 2021 **Andrew Wairegi**
<br>
You are free to view, but not copy.

[Open notebook]: https://gitlab.com/d7713/data-analysis/Analysis-Financial_inclusion/-/blob/main/Financial_inclusion_(east_africa)_Analysis.ipynb
